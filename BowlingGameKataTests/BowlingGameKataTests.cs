﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace BowlingGameKataTests
{
    [TestClass]
    public class BowlingGameKataTests
    {
        /// <summary>
        /// Überprüfung, ob der Score stimmt, wenn bei allen Runden nicht komplett abgeräumt wird
        /// </summary>
        [TestMethod]
        public void CheckForOnePinPerRoll()
        {
            var lGame = new BowlingGameKata.Game();

            for (var i = 1; i <= 20; i++)
                lGame.Roll(1);

            Assert.AreEqual(20, lGame.Score);
        }

        /// <summary>
        /// Überprüft den Score, wenn nur ein Strike geworfen wurde
        /// </summary>
        [TestMethod]
        public void CheckForSingleStrike()
        {
            var lGame = new BowlingGameKata.Game();

            lGame.Roll(1);
            lGame.Roll(1);
            lGame.Roll(10);
            lGame.Roll(5);
            lGame.Roll(1);

            for (var i = 6; i <= 20; i++)
                lGame.Roll(0);

            Assert.AreEqual(24, lGame.Score);
        }


        /// <summary>
        /// Überpüft den Score, wenn 3 Strikes geworfen wurden
        /// </summary>
        [TestMethod]
        public void CheckForThreeStrikes()
        {
            var lGame = new BowlingGameKata.Game();

            lGame.Roll(5);
            lGame.Roll(1);
            lGame.Roll(10);
            lGame.Roll(4);
            lGame.Roll(3);
            lGame.Roll(10);
            lGame.Roll(4);
            lGame.Roll(3);
            lGame.Roll(10);
            lGame.Roll(4);
            lGame.Roll(3);

            for (var i = 12; i <= 20; i++)
                lGame.Roll(0);

            Assert.AreEqual(78, lGame.Score);
        }

        /// <summary>
        /// Überprüft den Score, wenn in jeder Runde ein Strike geworfen wurde
        /// </summary>
        [TestMethod]
        public void CheckForPerfectGame()
        {
            var lGame = new BowlingGameKata.Game();

            for (var i = 1; i <= 10; i++)
                lGame.Roll(10);

            lGame.Roll(10);
            lGame.Roll(10);

            Assert.AreEqual(300, lGame.Score);
        }

        /// <summary>
        /// Überprüft den Score für einen geworfenen Spare im Spiel
        /// </summary>
        [TestMethod]
        public void CheckForSpare()
        {
            var lGame = new BowlingGameKata.Game();

            lGame.Roll(5);
            lGame.Roll(5);
            lGame.Roll(8);

            for (var i = 3; i <= 20; i++)
                lGame.Roll(0);

            Assert.AreEqual(26, lGame.Score);
        }

        /// <summary>
        /// Überprüft, ob der Score stimmt, wenn in keiner Runde ein Pin getroffen wurde
        /// </summary>
        [TestMethod]
        public void CheckForNoPinsHitAtAll()
        {
            var lGame = new BowlingGameKata.Game();

            for (var i = 1; i <= 20; i++)
                lGame.Roll(0);

            Assert.AreEqual(0, lGame.Score);
        }
    }
}
