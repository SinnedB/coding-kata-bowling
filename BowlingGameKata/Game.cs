﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGameKata
{
    public class Game
    {
        #region Props

        private Int32 _CurRoll { get; set; }

        /// <summary>
        /// Die Liste aller umgestoßenen Pins
        /// </summary>
        private List<Int32> _Rolls { get; set; }

        /// <summary>
        /// Gibt den Score aus
        /// </summary>
        public int Score
        {
            get
            {
                this._CurRoll = 0;
                Int32 lScore = 0;
                Int32 lRollIndex = 0;

                for (var ltRound = 0; ltRound < 10; ltRound++)
                {
                    if (this.zValidateStrike(lRollIndex))
                    {
                        // Bei einem Strike werden 10 Punkte verteilt ( für den Wurf ) plus die Punkte aus den nächsten beiden Wurf
                        lScore += 10 + this._Rolls.ElementAt(lRollIndex + 1) + this._Rolls.ElementAt(lRollIndex + 2);
                        lRollIndex += 1;
                    }
                    else if (this.zValidateSpare(lRollIndex))
                    {
                        // Bei einem Strike werden 10 Punkte verteilt ( für den Wert der beiden Würfe ) plus die Punkte aus dem nächsten Wurf
                        lScore += 10 + this._Rolls.ElementAt(lRollIndex + 2);
                        lRollIndex += 2;
                    }
                    else
                    {
                        lScore += this._Rolls.ElementAt(lRollIndex) + this._Rolls.ElementAt(lRollIndex + 1);
                        lRollIndex += 2;
                    }
                }

                return lScore;
            }
        }

        #endregion

        #region ctor

        /// <summary>
        /// Konstruktor
        /// </summary>
        public Game()
        {
            this._Rolls = new Int32[21].ToList();
        }

        #endregion

        #region public methods

        /// <summary>
        /// Führt einen Wurf mit der angegebenen Anzahl an umgeworfenen Pins an
        /// </summary>
        /// <param name="pRollValue"></param>
        public void Roll(Int32 pRollValue)
        {
            this._Rolls[_CurRoll++] = pRollValue;
        }

        #endregion

        #region private methods

        /// <summary>
        /// Überprüft, ob ein Strike in der angegeben Runde geworfen wurde
        /// </summary>
        /// <param name="pRollIndex"></param>
        /// <returns></returns>
        private bool zValidateStrike(Int32 pRollIndex)
        {
            return this._Rolls.ElementAt(pRollIndex) == 10;
        }

        /// <summary>
        /// Validiert, ob ein Spare in der angegeben Runde geworfen wurde
        /// </summary>
        /// <returns></returns>
        private Boolean zValidateSpare(Int32 pRollIndex)
        {
            // Überprüfen, ob die letzten beiden gerollten Werte 10 ergeben
            return this._Rolls.ElementAt(pRollIndex) + this._Rolls.ElementAt(pRollIndex + 1) == 10;
        }

        #endregion
    }
}
